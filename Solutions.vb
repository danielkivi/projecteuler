﻿Imports System.Diagnostics
Imports ProjectEuler.Common

Public Class Solutions
    Dim oWatch As New Stopwatch
    Dim helpers As Common = New Common

    Sub getAnswer(ByVal problem As String)
        Try
            CallByName(Me, "Problem" & problem, vbMethod)
            Console.WriteLine("Total Elapsed Time: " & oWatch.ElapsedMilliseconds)
            Console.Write(vbNewLine & "Another? (Y/N): ")
            oWatch.Reset()
        Catch
            Console.WriteLine("Not a valid problem")
        End Try

    End Sub

    Public Sub Problem1()
        Dim numbers As List(Of String) = New List(Of String)
        Dim bigNum As Double
        Dim multiples As List(Of Integer) = New List(Of Integer)
        Dim answer As Integer = 0
        Console.WriteLine("Problem 1: Sum of a list of all multiples under a number")
        Console.Write("Enter a comma delimated list of numbers to find multiples of: ")
        numbers = Split(Console.ReadLine(), ",").ToList()
        Console.Write("Enter the number to check multiples under: ")
        bigNum = Integer.Parse(Console.ReadLine())
        oWatch.Start()
        For Each number In numbers
            Dim current = 0
            Do
                current += Integer.Parse(number)
                If Not multiples.Contains(current) Then
                    multiples.Add(current)
                End If
            Loop While current + Integer.Parse(number) < bigNum
        Next
        For Each number As Integer In multiples
            answer += number
        Next
        oWatch.Stop()
        Console.WriteLine("Solution: " & answer)
    End Sub

    Public Sub Problem2()
        Dim largestNumber As Double
        Dim answer As Double = 0
        Console.WriteLine("Problem 2: Find the sum of even terms in the Fibonacci Series that do not exceed a number")
        Console.Write("Enter the number that terms may not exceed: ")
        largestNumber = Console.ReadLine()
        oWatch.Start()
        Dim lastTerm As Double = 0
        Dim count = 0
        While lastTerm < largestNumber
            lastTerm = helpers.FibonacciTerm(count)
            count += 1
            If lastTerm Mod 2 = 0 Then
                answer += lastTerm
            End If
        End While
        oWatch.Stop()
        Console.WriteLine("Solution: " & answer)
    End Sub

    Public Sub Problem3()
        Dim input As String
        Dim number As Double
        Console.WriteLine("Problem 3: Calculate the largest prime factor of a number")
        Console.Write("Enter a number to calculate largest prime, type default for original problem: ")
        input = Console.ReadLine
        oWatch.Start()
        If input.ToUpper = "DEFAULT" Then
            number = 600851475143
        Else
            number = Double.Parse(input)
        End If
        Dim pFactors As List(Of Integer) = helpers.PrimeFactors(number)
        Console.WriteLine("Solution: " & pFactors.Max())
        oWatch.Stop()
    End Sub

    Public Sub Problem4()
        Dim digits As Integer
        Dim answer As Integer = 0
        Console.WriteLine("Problem 4: Find the largest palindrome made from the product of two n-digit numbers")
        Console.Write("Enter how large how many digits the two factors are to be: ")
        digits = Integer.Parse(Console.ReadLine)
        oWatch.Start()
        Dim smallestNumber As Double = Math.Pow(10, digits - 1)
        Dim largestNumber As Double = Math.Pow(10, digits) - 1
        For x As Integer = largestNumber To smallestNumber Step -1
            For y As Integer = largestNumber To smallestNumber Step -1
                Dim possible As String = (x * y).ToString
                If possible = StrReverse(possible) Then
                    If Integer.Parse(possible) > answer Then
                        answer = possible
                    End If
                End If
            Next
        Next
        oWatch.Stop()
        Console.WriteLine("Solution: " & answer)
    End Sub

    Public Sub Problem5()
        Dim lastNumber As Integer
        Dim found As Boolean = False
        Console.WriteLine("Problem 5: Find the smallest number that is evenly divisble by all of the numbers between 1 and a number")
        Console.Write("Input the number to check all numbers from 1 to the number: ")
        lastNumber = Integer.Parse(Console.ReadLine())
        oWatch.Start()
        Dim count As Integer = lastNumber
        Do
            found = True
            For i As Integer = 2 To lastNumber
                If Not count Mod i = 0 Then
                    found = False
                    Exit For
                End If
            Next
            count += 1
        Loop While Not found
        oWatch.Stop()
        Console.WriteLine("Solution: " & count - 1)
    End Sub

    Public Sub Problem6()
        Dim nNumber As Integer
        Dim sumOfSquares As Long = 0
        Dim squareOfSum As Long = 0
        Console.WriteLine("Problem 6: Find the difference between the sum of squares of the first n natural numbers and the square of the sum of the first ten natural numbers")
        Console.Write("How many natural numbers to check: ")
        nNumber = Integer.Parse(Console.ReadLine)
        oWatch.Start()
        For i As Integer = 1 To nNumber
            squareOfSum += i
            sumOfSquares += i * i
        Next
        squareOfSum = squareOfSum * squareOfSum
        Dim answer As Long = squareOfSum - sumOfSquares
        oWatch.Stop()
        Console.WriteLine("Solution: " & answer)
    End Sub

    Public Sub Problem7()
        Dim primeIndex As Integer
        Dim input As String
        Console.WriteLine("Problem 7: Find the nth prime number")
        Console.Write("Enter what prime to find, type default for original problem: ")
        input = Console.ReadLine
        If input = "default" Then
            primeIndex = 10001
        Else
            primeIndex = Integer.Parse(input)
        End If
        oWatch.Start()
        Dim count As Integer = 1
        Dim number As Integer = 1
        While count < primeIndex
            number += 1
            If helpers.TestPrime(number) Then
                count += 1
            End If
        End While
        oWatch.Stop()
        Console.WriteLine("Solution: " & number)
    End Sub
End Class
