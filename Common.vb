﻿Public Class Common

    Public Function FibonacciTerm(ByVal index As Integer) As Integer
        If index = 0 Or index = 1 Then
            Return 1
        Else
            Return FibonacciTerm(index - 1) + FibonacciTerm(index - 2)
        End If
    End Function

    Public Function PrimeFactors(ByVal number As Double) As List(Of Integer)
        Dim progress As Double = number
        Dim primes As List(Of Integer) = New List(Of Integer)
        Dim factor As Integer = 2
        While Not progress = 1
            If progress Mod factor = 0 Then
                If TestPrime(factor) Then
                    If Not primes.Contains(factor) Then
                        primes.Add(factor)
                    End If
                    progress = progress / factor
                    factor += 1
                    Continue While
                End If
            End If
            factor += 1
        End While
        Return primes
    End Function

    Public Function TestPrime(ByVal number As Double) As Boolean
        If number = 1 Then
            Return False
        End If
        If number Mod 2 = 0 Then
            Return False
        End If
        Dim i As Integer = 3
        While i * i <= number
            If number Mod i = 0 Then
                Return False
            End If
            i += 2
        End While
        Return True
    End Function

End Class
